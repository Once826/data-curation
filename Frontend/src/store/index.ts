import Vue from 'vue';
import Vuex from 'vuex';
import VueApexCharts from 'vue-apexcharts';


Vue.use(Vuex);
Vue.use(VueApexCharts);
Vue.component('apexchart', VueApexCharts);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {},
});
