import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Data from "../views/Data.vue";
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Suggestions from "../views/Suggestions.vue"
import Database from "../views/Database.vue";
import Statistics from "../views/Statistics.vue"

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: "Home",
    component: Home,
  },
  {
    path: "/data",
    name: "Data",
    component: Data,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/suggestions",
    name: "Suggestions",
    component: Suggestions,
  },
  {
    path: "/database",
    name: "Database",
    component: Database,
  },
  {
    path: "/statistics",
    name: "Statistics",
    component: Statistics,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
