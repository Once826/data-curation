import http from "../http-common";

class DataService {
    getAll() {
      return http.get("/api");
    }
  
    getById(id: any) {
        return http.get(`/api/${id}`);
    }

    getTestMessage() {
        return http.get("/test");
    }

    getSameTexts(text: any) {
        return http.get(`/text/${text}`);
    }

    getImageToText() {
        return http.get(`/images`);
    }

    getAllCurated() {
        return http.get("/curated");
    }

    insertSuggestion(OCR_result_piece_id: any,suggested_text: any, OCR_text: any, conf_number: any, photo_reference: string, status: string) {
        const json = JSON.stringify({OCR_result_piece_id: OCR_result_piece_id, suggested_text: suggested_text, OCR_text: OCR_text, conf_number: conf_number, photo_reference: photo_reference, status: status})
        return http.post('/suggestions', json);
    }

    checkExistance(id: any){
        return http.get(`exists/${id}`);
    }

    getAllSuggestions(status: string) {
        return http.get('/suggestions',{ params: {
            status: status
        }});
    }

    deleteSuggestion(id: any) {
        return http.delete(`/suggestions/${id}`);
    }

    setStatus(id: any, status: string) {
        const json = JSON.stringify({ status:status })
        return http.put(`/status/${id}`,json)
    }

    getPending() {
        return http.get('/statistics/pending');
    }

    getApproved() {
        return http.get('/statistics/approved');
    }

    getDisapproved() {
        return http.get('/statistics/disapproved');
    }

    getAllOcrResults() {
        return http.get("/ocrResults")
    }
}
  
export default new DataService();
