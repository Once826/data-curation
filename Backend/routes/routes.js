const { ocr_result_pieces } = require("../models/index.js");

module.exports = app => {
    const OCR_result_pieces = require("../controllers/controller.js");
    var router = require("express").Router();

    router.get("/api/:ID", OCR_result_pieces.findOne);
    router.get("/test", OCR_result_pieces.testSend);

    router.get("/text/:text", OCR_result_pieces.findSameText);

    router.get("/exists/:id", OCR_result_pieces.checkIfExists);

    // /image -> body-ba megy egy JSON, ott van ID list
    router.get("/images", OCR_result_pieces.findAllImage);

    // PUT /sugestions -> body JSON {"OCR_result_piece_id": "5", "suggested_text":"ALMA"}
    router.post("/suggestions",OCR_result_pieces.insertSuggestion);
    
    // GET /sugestions -> JSON  {"statusfilter":[Pending]} // scak az kell, ami itt van, ha nincs is filter, akkor minden
    // GET /sugestions/{suggestionID}
    // GET /sugestions/{suggestionID}/status
    // POST /sugestions/{suggestionID}/status -> JSON newStatus: xxx
    router.get("/suggestions", OCR_result_pieces.findAllSuggestions);

    // PATCH (PUT, POST) /sugestions/{suggestionID}/status
    router.put("/status/:id",OCR_result_pieces.setStatus);

    // DELETE /sugestions/{suggestionID}
    router.delete("/suggestions/:id", OCR_result_pieces.deleteSuggestion);

    // GET /statistics - JSON {pendingSuggestionNr, approvedsUGGESTIONnUMber,...}
    router.get("/statistics/pending", OCR_result_pieces.getPending);
    router.get("/statistics/approved", OCR_result_pieces.getApproved);
    router.get("/statistics/disapproved", OCR_result_pieces.getDisapproved);
    app.use('/', router);
}