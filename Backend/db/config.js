module.exports = {
    //bakalada
    HOST: "localhost",
    USER: "bakalada-user",
    PASSWORD: "bakalada-pass",
    DB: "Bakalada",
    dialect: "mysql",
    pool: {
        max: 10
    },

    //bakalada-web
    HOST_2: "localhost",
    USER_2: "bakalada-user",
    PASSWORD_2: "bakalada-pass",
    DB_2: "Bakalada_web",
    dialect_2: "mysql",
    pool_2: {
        max: 10
    }
}
