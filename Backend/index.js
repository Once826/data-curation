const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const swaggerUI = require("swagger-ui-express");
const swaggerFile = require('./swagger_output.json');

const app = express();

const db = require("./models");

var corsOptions = {
    origin: "http://localhost:8080"
};

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerFile));

app.use(cors(corsOptions));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true}));

require('./routes/routes.js')(app);

const PORT = process.env.PORT || 8081;

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}\nAPI documentation: http://localhost:8081/api-docs`);
});