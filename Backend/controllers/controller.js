const db = require('../models');

const ocr_result_pieces = db.ocr_result_pieces;
const photo_segments = db.photo_segments;
const suggestions = db.suggestions;
const Op = db.Sequelize.Op;

exports.insertSuggestion = (req, res) => {
  // #swagger.tags = ['Suggestions']
  // #swagger.summary = 'Insert suggested text to the OCR_result_piece'
  // #swagger.description = 'Insert suggested text to the OCR_result_piece'
  const json = req.body
  const OCR_result_piece_id = json.OCR_result_piece_id
  const suggested_text = json.suggested_text
  const OCR_text = json.OCR_text
  const conf_number = json.conf_number
  const str = json.photo_reference.split("*")
  const photo_reference = str[0]+"/"+str[1]
  const status = json.status

  suggestions.create({
    OCR_result_piece_id: OCR_result_piece_id,
    suggested_text: suggested_text,
    OCR_text: OCR_text,
    conf_number: conf_number,
    photo_reference: photo_reference,
    status: status
  })
  .then(data => {
    /* #swagger.responses[200] = {
            description: 'Suggestion successfully created.',
    } */
    res.send(data);
  })
  .catch(err => {
      res.status(500).send({
          message:
              err.message || "Some error occurred while retrieving tutorials."
      });
  });
}


exports.checkIfExists =(req, res) => {
  // #swagger.tags = ['OCR_results']
  // #swagger.summary = 'Check if the OCR_result_pieces exists'
  // #swagger.description = 'Check if the OCR_result_pieces exists'
  /* #swagger.parameters['id'] = {
        in: 'path',
        required: true,
        type: 'integer',
        description: 'The OCR_result_piece id'
  }*/
  const id = req.params.id
  
  suggestions.count({
    where: {
      'OCR_result_piece_id' : id
    }
  }).then(data => {
    /* #swagger.responses[200] = {
            description: 'OCR_result_piece successfully obtained.',
    } */
    res.send({ value: data })
  })
  .catch(err => {
    res.status(500).send({
        message:
            err.message || "Some error occurred while retrieving tutorials."
    });
  });
}

exports.findAllImage = (req, res) => {
  // #swagger.tags = ['OCR_results']
  // #swagger.summary = 'Get all the images'
  // #swagger.description = 'Get all the images'
  ocr_result_pieces.findAll({
    include: [{
      model: photo_segments,
      required: true,
     }]
  }).then(data => {
    /* #swagger.responses[200] = {
            description: 'Images successfully obtained.',
    } */
    res.send(data);
  })
  .catch(err => {
      res.status(500).send({
          message:
              err.message || "Some error occurred while retrieving tutorials."
      });
  });
};

exports.findOne = (req, res) => {
  // #swagger.tags = ['OCR_results']
  // #swagger.summary = 'Get the OCR_result_pieces by ID'
  // #swagger.description = 'Get the OCR_result_pieces by ID'
  /* #swagger.parameters['ID'] = {
        in: 'path',
        required: true,
        type: 'integer',
        description: 'The OCR_result_piece id'
  }*/
  const ID = req.params.ID;

  ocr_result_pieces.findAll({
    include: [{
      model: photo_segments,
      required: true,
      }],
    where: {
      'ID' : ID
    }
  }).then(data => {
    /* #swagger.responses[200] = {
            description: 'OCR_result_piece successfully obtained.',
    } */
    res.send(data);
  })
  .catch(err => {
      res.status(500).send({
          message:
              err.message || "Some error occurred while retrieving tutorials."
      });
  });
};

exports.findSameText = (req, res) => {
  // #swagger.tags = ['OCR_results']
  // #swagger.summary = 'Get the OCR_result_pieces by text'
  // #swagger.description = 'Get the OCR_result_pieces by text'
  /* #swagger.parameters['text'] = {
        in: 'path',
        required: true,
        type: 'string',
        description: 'The OCR_result_piece text'
  }*/
  const text = req.params.text
  var condition = { OCR_text: text }

  ocr_result_pieces.findAll({
    where: condition ,
    include: [{
      model: photo_segments,
      required: true,
     }]
  })
  .then(data => {
    /* #swagger.responses[200] = {
            description: 'OCR_result_piece successfully obtained.',
    } */
      res.send(data);
  })
  .catch(err => {
      res.status(500).send({
          message:
              err.message || "Some error occurred while retrieving tutorials."
      });
  });
};

exports.testSend = (req, res) => {
  // #swagger.tags = ['Test']
  const message = "Hello from backend"

  res.send(message);
}

exports.findAllSuggestions = (req, res) => {
  // #swagger.tags = ['Suggestions']
  // #swagger.summary = 'Find all suggestions with given status'
  // #swagger.description = 'Find all suggestions with given status'
  const status = req.query.status
  suggestions.findAll({
    where: {
      'status' : status
    },
    order: [
      ['conf_number', 'ASC'],
    ],
  })
  .then(data => {
    /* #swagger.responses[200] = {
            description: 'Suggestions successfully obtained.',
    } */
      res.send(data);
  })
  .catch(err => {
      res.status(500).send({
          message:
              err.message || "Some error occurred while retrieving tutorials."
      });
  });
};

exports.deleteSuggestion = (req, res) => {
  // #swagger.tags = ['Suggestions']
  // #swagger.summary = 'Delete suggestion with given ID'
  // #swagger.description = 'Delete suggestion with given ID'
  /* #swagger.parameters['id'] = {
        in: 'path',
        required: true,
        type: 'integer',
        description: 'The Suggestion id'
  }*/
  const id = req.params.id
  console.log(id);
  suggestions.destroy({
    where: {
      'ID': id,
    }
  })
  .then(data => {
    /* #swagger.responses[200] = {
            description: 'Suggestion successfully removed.',
    } */
    res.sendStatus(200);
  })
  .catch(err => {
      res.status(500).send({
          message:
              err.message || "Some error occurred while retrieving tutorials."
      });
  });
}

exports.setStatus = (req, res) => {
  // #swagger.tags = ['Suggestions']
  // #swagger.summary = 'Set status for the given suggestion'
  // #swagger.description = 'Set status for the given suggestion'
  /* #swagger.parameters['text'] = {
        in: 'path',
        required: true,
        type: 'integer',
        description: 'The Suggestion id'
  }*/
  const id = req.params.id
  const status = req.body.status
  console.log(id,status);
  suggestions.update({ status: status }, {
    where: {
      'ID': id
    }
  })
  .then(data => {
    /* #swagger.responses[200] = {
            description: 'Suggestion status successfully modified.',
    } */
    res.sendStatus(200);
  })
  .catch(err => {
      res.status(500).send({
          message:
              err.message || "Some error occurred while retrieving tutorials."
      });
    });
}

exports.getPending = (req, res) => {
  // #swagger.tags = ['Statistics']
  // #swagger.summary = 'Get pending suggestions'
  // #swagger.description = 'Get pending suggestions'
  suggestions.count({
    where: {
      'status': 'pending',
    }
  })
  .then(data => {
    /* #swagger.responses[200] = {
            description: 'Pending suggestions successfully obtained.',
    } */
    res.send({ value: data});
  })
  .catch(err => {
    res.status(500).send({
        message:
            err.message || "Some error occurred while retrieving tutorials."
    });
  });
}

exports.getApproved = (req, res) => {
  // #swagger.tags = ['Statistics']
  // #swagger.summary = 'Get approved suggestions'
  // #swagger.description = 'Get approved suggestions'
  suggestions.count({
    where: {
      'status': 'approved',
    }
  })
  .then(data => {
    /* #swagger.responses[200] = {
            description: 'Approved suggestions successfully obtained.',
    } */
    res.send({ value: data});
  })
  .catch(err => {
    res.status(500).send({
        message:
            err.message || "Some error occurred while retrieving tutorials."
    });
  });
}

exports.getDisapproved = (req, res) => {
  // #swagger.tags = ['Statistics']
  // #swagger.summary = 'Get disapproved suggestions'
  // #swagger.description = 'Get disapproved suggestions'
  suggestions.count({
    where: {
      'status': 'disapproved',
    }
  })
  .then(data => {
    /* #swagger.responses[200] = {
            description: 'Disapproved suggestions successfully obtained.',
    } */
    res.send({ value: data});
  })
  .catch(err => {
    res.status(500).send({
        message:
            err.message || "Some error occurred while retrieving tutorials."
    });
  });
}